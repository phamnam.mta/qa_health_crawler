import requests
import json
import time
from tqdm import tqdm

BASE_URL = "https://isofhcare.com/_next/data/j4vruYokxgEQsdDpQcHYd/cong-dong.json?id={_id}"
index = 0 
max_request = 20000

question_answer = []
count = 0
for index in tqdm(range(max_request)[10000:20000]):
    url = BASE_URL.format(_id=index)
    data = requests.get(url).json()
    if not data["pageProps"]["data"]:
        count += 1
        print("data None", index)
    else:
        question_answer.append(data["pageProps"]["data"])
    if count >= 5:
        print(index)
        break
print(len(question_answer))
with open("/Users/namph/Documents/QA_health_crawler/isofhcare/data/raw_data2.json", "w") as file:
    json.dump(question_answer, file, ensure_ascii=False)

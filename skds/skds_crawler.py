from lxml import html
import requests
import re
import json
import time
from tqdm import tqdm

BASE_URL = "https://suckhoedoisong.vn"
QA_URL = "https://suckhoedoisong.vn/timelinelist/1691507/{index}.htm"
ACTION_URL = "https://s1.suckhoedoisong.vn/api-interview?id={_id}&action=1"
MAX_REQUEST = 193

question_answer = []
count = 0
for i in tqdm(range(MAX_REQUEST)):
    page = requests.get(QA_URL.format(index=i+1))
    tree = html.fromstring(page.content)
    href = tree.xpath('//a[@data-linktype="newsdetail"]/@href')
    href = list(dict.fromkeys(href))
    for h in tqdm(href):
        page = requests.get(BASE_URL+h)
        tree = html.fromstring(page.content)
        _id = tree.xpath('//input[@id="hdInterviewId"]/@value')

        if _id:
            content = requests.get(ACTION_URL.format(_id=_id[0]))
            content = json.loads(content._content)
            if content["data"].get("questions"):
                question_answer.append(content["data"])
                count += len(content["data"].get("questions"))
    
print(count)
with open("/Users/namph/Documents/qa_health_crawler/skds/data/raw_data.json", "w") as file:
    json.dump(question_answer, file, ensure_ascii=False)
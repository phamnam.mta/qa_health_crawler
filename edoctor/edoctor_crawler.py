import requests
import json
import time
from tqdm import tqdm

BASE_URL = "https://cms.edoctor.io/newsfeeds?_limit={limit}&_start={start}"
page_size = 20
index = 0 
max_request = 10000

question_answer = []
for index in tqdm(range(max_request)):
    url = BASE_URL.format(limit=page_size, start=index*page_size)
    data = requests.get(url).json()
    question_answer.extend(data["newsfeeds"])
    if len(data["newsfeeds"]) == 0:
        print(index)
        break
print(len(question_answer))
with open("/Users/namph/Documents/QA_health_crawler/edoctor/data/raw_data.json", "w") as file:
    json.dump(question_answer, file, ensure_ascii=False)

from lxml import html
import requests
import re
import json
from tqdm.contrib.concurrent import process_map
import time
from multiprocessing import RLock
from tqdm import tqdm
tqdm.set_lock(RLock())

BASE_URL = "https://www.vinmec.com/vi"
QA_URL = "https://www.vinmec.com/vi/tin-tuc/hoi-dap-bac-si/?page={index}"
MAX_REQUEST = 609

def process_data(i):
    data = []
    page = requests.get(QA_URL.format(index=i+1))
    tree = html.fromstring(page.content)
    href = tree.xpath('//a/@href')
    href = list(dict.fromkeys(href))
    for h in href:
        if re.search(r'/tin-tuc/hoi-dap-bac-si/.+', h):
            page = requests.get(BASE_URL+h)
            tree = html.fromstring(page.content)
            title = tree.xpath('//div[@class="detail-header"]//h1/text()[normalize-space()]')

            content_html = tree.xpath('//div[@class="rich-text"]//p')
            content = [' '.join(e.xpath('.//text()')) for e in content_html]
            data.append({
                "title": title,
                "content": content
            })
    return data


# question_answer = []
# for i in tqdm(range(MAX_REQUEST)):
#     page = requests.get(QA_URL.format(index=i+1))
#     tree = html.fromstring(page.content)
#     href = tree.xpath('//a/@href')
#     href = list(dict.fromkeys(href))
#     for h in href:
#         if re.search(r'/tin-tuc/hoi-dap-bac-si/.+', h):
#             page = requests.get(BASE_URL+h)
#             tree = html.fromstring(page.content)
#             #title = tree.xpath('//div[@class="detail-header"]//h1/text()')

#             content_html = tree.xpath('//div[@class="rich-text"]//p')
#             content = [' '.join(e.xpath('.//text()')) for e in content_html]
#             question_answer.append(content)

if __name__ == '__main__':
    data = process_map(process_data, range(MAX_REQUEST), max_workers=4, chunksize=32)
    data = [d for d in data if d]
    question_answer = []
    for d in data:
        if d:
            question_answer.extend(d)
    
    print(len(question_answer))
    with open("/Users/namph/Documents/qa_health_crawler/vinmec/data/raw_data_v2.json", "w") as file:
        json.dump(question_answer, file, ensure_ascii=False)
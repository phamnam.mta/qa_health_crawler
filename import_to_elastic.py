from elasticsearch import Elasticsearch, helpers
import json

es = Elasticsearch()

def load_json():
    " Use a generator, no need to load all in memory"
    with open("/Users/namph/Documents/qa_health_crawler/data/all/QA.json",'r') as open_file:
        yield json.load(open_file)

helpers.bulk(es, load_json(), index='my-index', doc_type='my-type')
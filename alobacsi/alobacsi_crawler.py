from lxml import html
import requests
import re
import json
from tqdm.contrib.concurrent import process_map
import time
from multiprocessing import RLock
from tqdm import tqdm
tqdm.set_lock(RLock())

BASE_URL = "https://alobacsi.com/kham-benh-online/p-{index}/"
MAX_REQUEST = 2358
URL_SEEN = []
URL_FAILED = []

def crawl_data(h):
    data = []
    page = requests.get(h)
    tree = html.fromstring(page.content)
    title = tree.xpath('//div[@class="col-lg-12"]//h1/text()')
    if title:
        tag = tree.xpath('//h6[@class="text-primary text-uppercase"]/text()')
        tag = tag[0] if tag else ""
        title = title[0]
        question_html = tree.xpath('//div[@class="p-3 bg-primary-o rounded"]//p')
        question = [' '.join(e.xpath('.//text()')) for e in question_html]
        if not question:
            question = tree.xpath('//div[@class="p-3 bg-primary-o rounded"]//text()[normalize-space()]')
        
        answer = tree.xpath('//div[@class="main-detail mb-3"]//text()[normalize-space()]')
        data.append({
            "title": title,
            "tag": tag,
            "question": question,
            "answer": answer
        })
        if not question or not answer:
            URL_FAILED.append(h)
            print(h)
    URL_SEEN.append(h)
    return data

def get_page_url(i):
    page = requests.get(BASE_URL.format(index=i+1))
    tree = html.fromstring(page.content)
    href = tree.xpath('//a/@href')
    return href

if __name__ == '__main__':
    question_answer = []
    # href = process_map(get_page_url, range(MAX_REQUEST), max_workers=4, chunksize=64)
    # urls = []
    # for h in href:
    #     urls.extend(h)
    # urls = [u for u in urls if u]
    # urls = list(dict.fromkeys(urls))
    # urls = [u for u in urls if re.search(r'https://alobacsi.com/kham-benh-online/.+', u) and not re.search(r'kham-benh-online/p-\d+', u)]
    
    # with open("/home/phamnam/Documents/QA_health_crawler/alobacsi/data/urls.txt", "w") as file:
    #     file.write("\n".join(urls))
    
    with open("/Users/namph/Documents/qa_health_crawler/alobacsi/data/urls.txt", "r") as file:
        lines = file.readlines()
        urls = [line.rstrip() for line in lines]
    print("urls", len(urls))
    try:
        data = process_map(crawl_data, urls, max_workers=4, chunksize=16)
        for d in data:
            if d:
                question_answer.extend(d)
        
        print(len(question_answer))
        with open("/Users/namph/Documents/qa_health_crawler/alobacsi/data/raw_data.json", "w") as file:
            json.dump(question_answer, file, ensure_ascii=False)
        with open("/Users/namph/Documents/qa_health_crawler/alobacsi/data/failed_urls.txt", "w") as file:
            file.write("\n".join(URL_FAILED))
    except:
        with open("/Users/namph/Documents/qa_health_crawler/alobacsi/data/seen_urls.txt", "w") as file:
            file.write("\n".join(URL_SEEN))
        with open("/Users/namph/Documents/qa_health_crawler/alobacsi/data/raw_data.json", "w") as file:
            json.dump(question_answer, file, ensure_ascii=False)
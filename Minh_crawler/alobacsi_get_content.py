from selenium import webdriver
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from tqdm import tqdm
import pandas as pd
import time
import json
DRIVER_PATH = '/home/minh/work/vinbrain/vi-MedBERT/crawl_data/chromedriver_linux64/chromedriver'
driver = webdriver.Chrome(executable_path=DRIVER_PATH)
options = Options()
data = pd.read_csv('alobacsi.csv')
links = data['link'].tolist()
questions = data['title'].tolist()    
CHUNK_SIZE = 100
chunk_cnt = 1

url = []
titles = []
content = []
res = []
fail_url = []
for link,ques in tqdm(zip(links,questions)):
    driver.get(link)
    try:
        contents = driver.find_element_by_xpath('//*/div[@class="post-content pt-3"]/div[@class="main-detail mb-3"]')
        contents = contents.find_elements_by_tag_name('p')
    except:
        print(link)
        continue
    tmp_c = []
    for c in contents:
        tmp_c.append(c.text)
    
    # tmp = tmp[:-1]
    res.append({
        'url': link,
        'question': ques,
        'answer': tmp_c,
    })

    if len(res) == CHUNK_SIZE:
        with open(f'chunk_data/alobacsi_part{chunk_cnt}.json', 'w',encoding='utf-8') as f:
            json.dump(res, f,ensure_ascii=False)
        print(f'Done chunk {chunk_cnt} : {len(res)}')

        chunk_cnt += 1
        res = []
with open(f'chunk_data/alobacsi_part{chunk_cnt}.json', 'w',encoding='utf-8') as f:
    json.dump(res, f,ensure_ascii=False)
driver.quit()

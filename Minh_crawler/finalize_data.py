import os
import json

files = os.listdir('chunk_data')
print(len(files))

print(files[0])

final_data = []
for ff in files:
    with open(f'chunk_data/{ff}') as f:
        data = json.load(f)
    for sample in data:
        if sample not in final_data:
            final_data.append(sample)
        

print(len(final_data))
with open('alobacsi_data.json', 'w',encoding='utf-8') as f:
    json.dump(final_data, f,ensure_ascii=False)